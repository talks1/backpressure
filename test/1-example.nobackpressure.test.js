import { describe } from 'riteway'
const debug = require('debug')('test')
const { Duplex, Writable } = require('stream')
let csvparse = require('csv-parse')
let fs = require('fs')


let buffer = [] //Buffering the data read...required because of a slow 'mapper'

let slowmapper = new Duplex({
  objectMode: true, //Because we are dealing with json objects rather than byte arrays
  write (chunk, encoding, callback) {
      debug(`Write CSV ${buffer.length}`)
      buffer.push(chunk) 
      callback()  
  },
  read () {
      setTimeout(() => {
        debug(`Read Mapped Value: HeapTotal${process.memoryUsage().heapTotal}`)
        //There is no actual transformation happening but could happen here
        this.push(buffer.shift())
      }, 1000) //This slow mapper only reads one value each second 
  },
})

let showstream = new Writable({
  objectMode: true,
  write (chunk, encoding, callback) {
      debug(`Show Mapped Value ${buffer.length}`)
      callback() 
  }
})

describe.only('1.example.no backpressure', async (assert) => {
  let readstream = fs.createReadStream('./test/large-house-data.csv')
    .pipe(csvparse())
    .pipe(slowmapper)
    .pipe(showstream)    
})
