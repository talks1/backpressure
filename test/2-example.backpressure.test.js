import { describe } from 'riteway'
const debug = require('debug')('test')
const { Duplex, Writable } = require('stream')

let csvparse = require('csv-parse')
let fs = require('fs')

let DELAY=1000
let HIGH_WATER_MARK=20

let buffer = []

// This applies the back pressure where be slow down a READER to match a WRITER
// A Writeable signal it can accept more data to an upstream Readable through delaying the callback response for any one piece of data.
let checkMark = (duplex, delay, highWaterMark, callback) => {
  let mark = buffer.length
  if (mark >= highWaterMark) {
      setTimeout(checkMark, delay, duplex, delay, highWaterMark, callback)
  } else {
      callback()
  }
}

let slowmapper = new Duplex({
  objectMode: true,
  write (chunk, encoding, callback) {
      debug(`Write CSV ${buffer.length}`)
      buffer.push(chunk)
      checkMark(this,DELAY,HIGH_WATER_MARK,callback)  
  },
  read (n) {
      setTimeout(() => {
        debug(`Read Mapped Value: HeapTotal${process.memoryUsage().heapTotal}`)
        this.push(buffer.shift())
      }, 1000)  
  },
})

let showstream = new Writable({
  objectMode: true,
  write (chunk, encoding, callback) {
      debug(`Show Mapped Value ${buffer.length}`)
      callback() 
  }
})

describe('2.example.backpressure', async (assert) => {
  let readstream = fs.createReadStream('./test/large-house-data.csv')
    .pipe(csvparse())
    .pipe(slowmapper)
    .pipe(showstream)    
})
